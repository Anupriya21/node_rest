
const express = require('express');
const app = express();
const port = 3000;
const bodyParser = require('body-parser');
// create application/x-www-form-urlencoded parser
app.use(bodyParser.urlencoded({ extended: true }));
// create application/json parser
app.use(bodyParser.json());
app.use((request, response, next) => {
  response.header("Access-Control-Allow-Origin", "*");
  response.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  //Cross-Origin Resource Sharing (CORS) is a mechanism that uses additional HTTP headers to tell a browser to let a web application running at one
  //origin (domain) have permission to access selected resources from a server at a different origin.
  response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const contactsController = require('./controller/controller')();
app.use("/api/contacts", contactsController);
app.listen(port, function () {
    let datetime = new Date();
    let message = "Server runnning on Port:- " + port + "Started at :- " + datetime;
    console.log(message);
});
