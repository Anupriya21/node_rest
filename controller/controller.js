const express = require("express");
const router = express.Router();
const sql = require("mssql");
const connection = require("../connection/connection")();
const routes = function () {

    router.route('/')
        .get(function (req, res) {
            connection.connect().then(function () {
              let sqlQuery = "SELECT * FROM contacts";
              let req = new sql.Request(connection);
              req.query(sqlQuery).then(function (recordset) {
                  res.json(recordset.recordset);
                  connection.close();
              })
              .catch(function (err) {
                  connection.close();
                  res.status(400).send("Database Error while getting data" + err);
              });
            })
            .catch(function (err) {
                connection.close();
                res.status(400).send("Connection Error while getting data" + err);
            });
          });
  router.route('/echo')
         .get(function(req,res){
            res.send(req.query.text.toUpperCase());
         });
    router.route('/')
    .post(function (req, res) {
        connection.connect().then(function () {
            let transaction = new sql.Transaction(connection);
            transaction.begin().then(function () {
                let request = new sql.Request(transaction);
                request.input("name", sql.VarChar(50), req.body.name);
                request.input("phone", sql.VarChar(20), req.body.phone); //request.query("INSERT INTO contacts(name, phone) VALUES(@name,@phone)").then(function () {
                request.execute("spAddContact").then(function () { // Use a Stored Procedure
                    transaction.commit().then(function (recordSet) {
                        connection.close();
                        res.status(200).send(req.body);
                    }).catch(function (err) {
                        connection.close();
                        res.status(400).send("Transaction Commit Error while inserting data");
                    });
                }).catch(function (err) {
                    connection.close();
                    res.status(400).send("Database Error while inserting data");
                });
            }).catch(function (err) {
                connection.close();
                res.status(400).send("Transaction Begin Error while inserting data");
            });
        }).catch(function (err) {
            connection.close();
            res.status(400).send("Connection Error while inserting data");
        });
    });

    router.route('/')
    .put(function (req, res) {
        connection.connect().then(function () {
            let transaction = new sql.Transaction(connection);
            transaction.begin().then(function () {
                let request = new sql.Request(transaction);
                request.input("name", sql.VarChar(50), req.body.name);
                request.input("phone", sql.VarChar(20), req.body.phone);
                request.input("id", sql.Int, parseInt(req.body.id));
                request.query("UPDATE contacts SET name = @name, phone=@phone WHERE id = @id").then(function () { // Use a Stored Procedure
                    transaction.commit().then(function (recordSet) {
                        connection.close();
                        res.status(200).send(req.body);
                    }).catch(function (err) {
                        connection.close();
                        res.status(400).send("Transaction Commit Error while inserting data");
                    });
                }).catch(function (err) {
                    connection.close();
                    res.status(400).send("Database Error while inserting data");
                });
            }).catch(function (err) {
                connection.close();
                res.status(400).send("Transaction Begin Error while inserting data");
            });
        }).catch(function (err) {
            connection.close();
            res.status(400).send("Connection Error while inserting data");
        });
    });

    router.route('/:id')
    .delete(function (req, res) {
        let _id = req.params.id;
        connection.connect().then(function () {
            let transaction = new sql.Transaction(connection);
            transaction.begin().then(function () {
                let request = new sql.Request(transaction);
                request.input("id", sql.Int, _id);
                request.query("DELETE FROM contacts WHERE id = @id").then(function () { // Use a Stored Procedure
                    transaction.commit().then(function (recordSet) {
                        connection.close();
                        res.status(200).send(req.body);
                    }).catch(function (err) {
                        connection.close();
                        res.status(400).send("Transaction Commit Error while removing data");
                    });
                }).catch(function (err) {
                    connection.close();
                    res.status(400).send("Database Error while deleting data");
                });
            }).catch(function (err) {
                connection.close();
                res.status(400).send("Transaction Begin Error while deleting data");
            });
        }).catch(function (err) {
            connection.close();
            res.status(400).send("Connection Error while deleting data");
        });
    });

    router.route('/:id')
        .get(function (req, res) {
            let _id = req.params.id;
            connection.connect().then(function () {
              let sqlQuery = "SELECT * FROM contacts WHERE id =@id";
              let req = new sql.Request(connection);
              req.input("id", sql.Int, _id);
              req.query(sqlQuery).then(function (recordset) {
                  res.json(recordset.recordset);
                  connection.close();
              })
              .catch(function (err) {
                  connection.close();
                  res.status(400).send("Database Error while getting data" + err);
              });
            })
            .catch(function (err) {
                connection.close();
                res.status(400).send("Connection Error while getting data" + err);
            });
          });

          //Excercise using a View or a Stored Procedure list the  orders (Company name,
        // OrderID, ShipVia and a formatted order date CAST(OrderDate as varchar(11))
        // for a customer (Use the CustomerID eg ANTON)
        router.route('/customer/:cid')
            .get(function (req, res) {
                let customerid = req.params.cid;
                connection.connect().then(function () {
                  let sqlQuery = "SELECT * FROM vCustomerOrders WHERE CustomerID = @custid";
                  let req = new sql.Request(connection);
                  req.input("custid", sql.Char(5), customerid);
                  req.query(sqlQuery).then(function (recordset) {
                      res.json(recordset.recordset);
                      connection.close();
                  })
                  .catch(function (err) {
                      connection.close();
                      res.status(400).send("Database Error while getting data" + err);
                  });
                })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data" + err);
                });
              });

              router.route('/Shippers')
              .post(function (req, res) {
                  connection.connect().then(function () {
                      let transaction = new sql.Transaction(connection);
                      transaction.begin().then(function () {
                          let request = new sql.Request(transaction);
                          request.input("CompanyName", sql.VarChar(50), req.body.CompanyName);
                          request.input("Phone", sql.VarChar(50), req.body.Phone);
                          request.query("INSERT INTO Shippers(CompanyName, Phone) VALUES(@CompanyName,@Phone)").then(function () {
                          //request.execute("spAddShipper").then(function () { // Use a Stored Procedure
                              transaction.commit().then(function (recordSet) {
                                  connection.close();
                                  res.status(200).send(req.body);
                              }).catch(function (err) {
                                  connection.close();
                                  res.status(400).send("Transaction Commit Error while inserting data");
                              });
                          }).catch(function (err) {
                              connection.close();
                              res.status(400).send("Database Error while inserting data");
                          });
                      }).catch(function (err) {
                          connection.close();
                          res.status(400).send("Transaction Begin Error while inserting data");
                      });
                  }).catch(function (err) {
                      connection.close();
                      res.status(400).send("Connection Error while inserting data");
                  });
              });

              router.route('/Shippers')
              .put(function (req, res) {
                  connection.connect().then(function () {
                      let transaction = new sql.Transaction(connection);
                      transaction.begin().then(function () {
                          let request = new sql.Request(transaction);
                          request.input("CompanyName", sql.VarChar(50), req.body.CompanyName);
                          request.input("Phone", sql.VarChar(20), req.body.Phone);
                          request.input("ShipperID", sql.Int, parseInt(req.body.ShipperID));
                          request.query("UPDATE Shippers SET CompanyName = @CompanyName, Phone=@Phone WHERE ShipperID = @ShipperID").then(function () { // Use a Stored Procedure
                              transaction.commit().then(function (recordSet) {
                                  connection.close();
                                  res.status(200).send(req.body);
                              }).catch(function (err) {
                                  connection.close();
                                  res.status(400).send("Transaction Commit Error while inserting data");
                              });
                          }).catch(function (err) {
                              connection.close();
                              res.status(400).send("Database Error while inserting data");
                          });
                      }).catch(function (err) {
                          connection.close();
                          res.status(400).send("Transaction Begin Error while inserting data");
                      });
                  }).catch(function (err) {
                      connection.close();
                      res.status(400).send("Connection Error while inserting data");
                  });
              });


              router.route('/customer')
                          .post(function (req, res) {
                              connection.connect().then(function () {
                                let request = new sql.Request(connection);
                                console.log(req.body.country);
                                request.input("country",sql.VarChar(40),req.body.country);
                                request.input("year",sql.Int,parseInt(req.body.year));
                                // stored procedure --request.execute("spCountryYearSales").then(function (recordset) {

                                    //res.json(recordset.recordset);
                                  //  connection.close();
                              //  })
                                request.query("SELECT CompanyName, Sales FROM vCountryYearSales WHERE Year =@year AND Country = @country").then(function(recordset){
                                  res.json(recordset.recordset);
                                  connection.close();
                                })
                                .catch(function (err) {
                                    connection.close();
                                    res.status(400).send("Database Error while getting data " + err);
                                });
                              })
                              .catch(function (err) {
                                  connection.close();
                                  res.status(400).send("Connection Error while getting data " + err);
                              });
                            });


                            router.route('/customer')
                                        .get(function (req, res) {
                                            connection.connect().then(function () {
                                              let request = new sql.Request(connection);
                                              console.log(req.query.country);
                                              request.input("country",sql.VarChar(40),req.query.country);
                                              request.input("year",sql.Varchar(10),req.query.year);
                                              // stored procedure --request.execute("spCountryYearSales").then(function (recordset) {

                                                  //res.json(recordset.recordset);
                                                //  connection.close();
                                            //  })
                                              request.query("SELECT CompanyName, Sales FROM vCountryYearSales WHERE Year =@year AND Country = @country").then(function(recordset){
                                                res.json(recordset.recordset);
                                                connection.close();
                                              })
                                              .catch(function (err) {
                                                  connection.close();
                                                  res.status(400).send("Database Error while getting data " + err);
                                              });
                                            })
                                            .catch(function (err) {
                                                connection.close();
                                                res.status(400).send("Connection Error while getting data " + err);
                                            });
                                          });



    return router;
};
module.exports = routes;
